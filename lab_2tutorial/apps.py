from django.apps import AppConfig


class Lab2TutorialConfig(AppConfig):
    name = 'lab_2tutorial'
