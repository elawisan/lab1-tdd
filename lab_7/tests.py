from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index
from .api_csui_helper.csui_helper import CSUIhelper
import unittest

# Create your tests here.
@unittest.skip("skip the test")
class Lab7UnitTest(TestCase):
    def test_lab_7_url_is_exist(self):
        response = Client().get('/lab-7/')
        self.assertEqual(response.status_code, 200)

    def test_lab7_using_index_func(self):
        found = resolve('/lab-7/')
        self.assertEqual(found.func, index)
		
    def test_lab7_pagination(self):
        response = Client().get('/lab-7/')
        html_response = response.content.decode('utf8')
        self.assertIn("HUGO REYNALDO (0606101452)",html_response)
            
    def test_lab_7_friend_list_is_exist(self):
        response = Client().get('/lab-7/get-friend-list/')
        html_response = response.content.decode('utf8')
        self.assertEqual(response.status_code, 200)
                    
    def test_csui_helper(self):
        CSUIhelper()
		
                    
		
		
