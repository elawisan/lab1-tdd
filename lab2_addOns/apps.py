from django.apps import AppConfig


class Lab2AddonsConfig(AppConfig):
    name = 'lab2_addOns'
